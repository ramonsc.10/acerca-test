using System.Collections.Generic;

namespace App.Models.Vehicles
{
    public class GetVehicleTableInformationResponse
    {
        public GetVehicleTableInformationResponse(
            int totalItems,
            IEnumerable<Vehicle> vehicles)
        {
            TotalItems = totalItems;
            Vehicles = vehicles;
        }

        public int TotalItems { get; set; }
        public IEnumerable<Vehicle> Vehicles { get; set; }
    }
}