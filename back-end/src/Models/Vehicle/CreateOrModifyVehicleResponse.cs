namespace App.Models.Vehicles
{
    public class CreateOrModifyVehicleResponse
    {
        public Vehicle Vehicle { get; set; } = default!;
    }
}