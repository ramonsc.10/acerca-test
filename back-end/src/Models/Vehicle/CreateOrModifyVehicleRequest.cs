namespace App.Models.Vehicles
{
    public class CreateOrModifyVehicleRequest
    {
        public CreateOrModifyVehicleRequest()
        {
            UpsertVehicle = new UpsertVehicle();
        }

        public UpsertVehicle UpsertVehicle { get; set; }

    }
}