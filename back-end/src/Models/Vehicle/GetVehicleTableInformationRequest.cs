namespace App.Models.Vehicles
{
    public class GetVehicleTableInformationRequest
    {
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        
    }
}