using System;

namespace App.Models.Vehicles
{
    public class DeleteVehicleRequest
    {
        public Guid Id { get; set; }
    }
}