using System;

namespace App.Models.Vehicles
{
    public class UpsertVehicle
    {
        public UpsertVehicle()
        {
            Model = string.Empty;
            Chassis = string.Empty;
            Plate = string.Empty;
        }

        public UpsertVehicle(
            Guid? id,
            int orderNumber,
            string model,
            string chassis,
            string plate,
            DateTime deliveryDate)
        {
            Id = id;
            OrderNumber = orderNumber;
            Model = model;
            Chassis = chassis;
            Plate = plate;
            DeliveryDate = deliveryDate;
        }

        public Guid? Id { get; set; }
        public int OrderNumber { get; set; }
        public string Model { get; set; }
        public string Chassis { get; set; }
        public string Plate { get; set; }
        public DateTime DeliveryDate { get; set; }
    }
}