using System;

namespace App.Models.Vehicles
{
    public class GetVehicleInformationRequest
    {
        public Guid Id { get; set; }
    }
}