namespace App.Models.Vehicles
{
    public class GetVehicleInformationResponse
    {
        public GetVehicleInformationResponse(Vehicle? vehicle)
        {
            Vehicle = vehicle;
        }

        public Vehicle? Vehicle { get; set; }
    }
}