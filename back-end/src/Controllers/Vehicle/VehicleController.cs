using System.Threading.Tasks;
using App.Models.Vehicles;
using App.Services.Vehicles;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class VehicleController : ControllerBase
    {
        private readonly IVehicleService vehicleService;

        public VehicleController(IVehicleService vehicleService)
        {
            this.vehicleService = vehicleService;
        }

        [AllowAnonymous]
        [HttpGet]
        public Task<GetVehicleInformationResponse> GetVehicleInformation([FromQuery] GetVehicleInformationRequest request)
        {
            return vehicleService.GetVehicleInformationAsync(request);
        }

        [AllowAnonymous]
        [HttpGet("Table")]
        public Task<GetVehicleTableInformationResponse> GetVehicleTableInformation([FromQuery] GetVehicleTableInformationRequest request)
        {
            return vehicleService.GetVehicleTableInformationAsync(request);
        }

        [AllowAnonymous]
        [HttpPost]
        public Task<CreateOrModifyVehicleResponse> CreateOrModifyVehicle([FromBody] CreateOrModifyVehicleRequest request)
        {
            return vehicleService.CreateOrModifyVehicleAsync(request);
        }

        [AllowAnonymous]
        [HttpDelete]
        public Task DeleteVehicle([FromQuery] DeleteVehicleRequest request)
        {
            return vehicleService.DeleteVehicleAsync(request);
        }

    }
}