using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using App.Models.Vehicles;

namespace App.Respositories.Vehicles
{
    public interface IVehicleRepository
    {
        Task<int> GetTotalNumberOfVehiclesAsync();
        Task<IEnumerable<Vehicle>> GetVehiclesOfThePageAsync(int pageSize, int pageNumber);
        Task<Vehicle> InsertAsync(UpsertVehicle upsertVehicle);
        Task<Vehicle> UpdateAsync(UpsertVehicle upsertVehicle);
        Task<Vehicle?> FindByIdAsync(Guid id);
        Task RemoveAsync(Guid id);
    }
}