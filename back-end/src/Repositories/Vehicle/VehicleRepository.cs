using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.Context;
using App.Mappers.Vehicles;
using App.Models.Vehicles;
using Microsoft.EntityFrameworkCore;

namespace App.Respositories.Vehicles
{
    public class VehicleRepository : IVehicleRepository
    {
        private readonly ApplicationContext context;
        private readonly IVehicleMapper mapper;

        public VehicleRepository(
            ApplicationContext appContext,
            IVehicleMapper mapper)
        {
            context = appContext;
            this.mapper = mapper;
        }

        public async Task<Vehicle?> FindByIdAsync(Guid id)
        {
            var dbRegister = await context.Vehicles.FindAsync(id);
            if (dbRegister == null)
            {
                return null;
            }

            return mapper.From(dbRegister);
        }

        public async Task<int> GetTotalNumberOfVehiclesAsync()
        {
            return await context.Vehicles
                .AsNoTracking()
                .CountAsync();
        }

        public async Task RemoveAsync(Guid id)
        {
            var dbRegister = await context.Vehicles.FindAsync(id);
            if (dbRegister != null)
            {
                context.Vehicles.Remove(dbRegister);
                await context.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<Vehicle>> GetVehiclesOfThePageAsync(int pageSize, int pageNumber)
        {
            var query = context.Vehicles
                .AsNoTracking();

            if (pageSize > 0)
            {
                query = query.Skip(pageSize * pageNumber).Take(pageSize);
            }

            return await query
                .Select(vehicle => mapper.From(vehicle))
                .ToListAsync();
        }

        public async Task<Vehicle> InsertAsync(UpsertVehicle upsertVehicle)
        {
            var dbRegister = mapper.From(upsertVehicle);

            await context.Vehicles.AddAsync(dbRegister);
            await context.SaveChangesAsync();

            return mapper.From(dbRegister);
        }

        public async Task<Vehicle> UpdateAsync(UpsertVehicle upsertVehicle)
        {
            var dbRegister = mapper.From(upsertVehicle);

            context.Attach(dbRegister);
            context.Entry(dbRegister).State = EntityState.Modified;
            await context.SaveChangesAsync();

            return mapper.From(dbRegister);
        }
    }
}