using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using App.Context;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using App.Respositories.Vehicles;
using App.Services.Vehicles;
using App.Mappers.Vehicles;

namespace App
{
    public class Startup
    {
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationContext>(opt => opt.UseNpgsql(CreateDbConnectionString()));

            services.AddControllersWithViews();

            services.AddCors(options =>
                    {
                        options.AddPolicy(name: MyAllowSpecificOrigins,
                                          builder =>
                                          {
                                              builder
                                                .AllowAnyOrigin()
                                                .AllowAnyHeader()
                                                .AllowAnyMethod();
                                          });
                    });

            services.AddTransient<IVehicleRepository, VehicleRepository>();
            services.AddTransient<IVehicleService, VehicleService>();
            services.AddTransient<IVehicleMapper, VehicleMapper>();


        }

        private string CreateDbConnectionString()
        {
            var connectionStringBuilder = new NpgsqlConnectionStringBuilder();

            var uri = Configuration.GetValue<Uri>("DATABASE_URL");

            if (uri != null)
            {
                var userInfoParts = uri.UserInfo.Split(':');
                var userName = userInfoParts[0];
                var password = userInfoParts[1];
                var databaseName = uri.LocalPath.Substring(1);

                connectionStringBuilder.Host = uri.Host;
                connectionStringBuilder.Port = uri.Port;
                connectionStringBuilder.Database = databaseName;
                connectionStringBuilder.Username = userName;
                connectionStringBuilder.Password = password;
            }
            else
            {
                connectionStringBuilder.Host = Configuration.GetValue<string>("Database:Host");
                connectionStringBuilder.Port = Configuration.GetValue<int>("Database:Port");
                connectionStringBuilder.Database = Configuration.GetValue<string>("Database:Name");
                connectionStringBuilder.Username = Configuration.GetValue<string>("Database:UserName");
                connectionStringBuilder.Password = Configuration.GetValue<string>("Database:Password");
            }

            var sslMode = Configuration.GetValue<SslMode?>("Database:SslMode");
            if (sslMode != null)
            {
                connectionStringBuilder.SslMode = sslMode.Value;
            }

            var trustServerCertificate = Configuration.GetValue<bool?>("Database:TrustServerCertificate");
            if (trustServerCertificate != null)
            {
                connectionStringBuilder.TrustServerCertificate = trustServerCertificate.Value;
            }

            return connectionStringBuilder.ToString();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }


            if (Configuration.GetSection("LettuceEncrypt").Exists())
            {
                app.UseHsts();
                app.UseHttpsRedirection();
            }

            app.UseRouting();

            app.UseCors(MyAllowSpecificOrigins);

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

        }
    }
}
