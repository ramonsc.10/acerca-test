using System;
using App.Models.Vehicles;

namespace App.Mappers.Vehicles
{
    public class VehicleMapper : IVehicleMapper
    {
        public Vehicle From(VehicleEfModel DbRegister)
        {
            return new Vehicle(
                DbRegister.Id,
                DbRegister.OrderNumber,
                DbRegister.Model,
                DbRegister.Chassis,
                DbRegister.Plate,
                DbRegister.DeliveryDate
            );
        }

        public VehicleEfModel From(UpsertVehicle upsertVehicle)
        {
            return new VehicleEfModel(
                upsertVehicle.Id.HasValue ? upsertVehicle.Id.Value : Guid.NewGuid(),
                upsertVehicle.OrderNumber,
                upsertVehicle.Model,
                upsertVehicle.Chassis,
                upsertVehicle.Plate,
                upsertVehicle.DeliveryDate
            );
        }
    }
}