using App.Models.Vehicles;

namespace App.Mappers.Vehicles
{
    public interface IVehicleMapper
    {
        Vehicle From(VehicleEfModel DbRegister);
        VehicleEfModel From(UpsertVehicle upsertVehicle);
    }
}