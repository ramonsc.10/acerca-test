using System.Threading.Tasks;
using App.Models.Vehicles;
using App.Respositories.Vehicles;

namespace App.Services.Vehicles
{
    public class VehicleService : IVehicleService
    {
        private readonly IVehicleRepository vehicleRepository;

        public VehicleService(IVehicleRepository vehicleRepository)
        {
            this.vehicleRepository = vehicleRepository;
        }

        public async Task<CreateOrModifyVehicleResponse> CreateOrModifyVehicleAsync(CreateOrModifyVehicleRequest request)
        {
            var response = new CreateOrModifyVehicleResponse();

            if (request.UpsertVehicle.Id.HasValue)
            {
                response.Vehicle = await vehicleRepository.UpdateAsync(request.UpsertVehicle);
            }
            else
            {
                response.Vehicle = await vehicleRepository.InsertAsync(request.UpsertVehicle);
            }

            return response;
        }

        public async Task DeleteVehicleAsync(DeleteVehicleRequest request)
        {
            await vehicleRepository.RemoveAsync(request.Id);
        }

        public async Task<GetVehicleInformationResponse> GetVehicleInformationAsync(GetVehicleInformationRequest request)
        {
            var vehicle = await vehicleRepository.FindByIdAsync(request.Id);

            return new GetVehicleInformationResponse(vehicle);
        }

        public async Task<GetVehicleTableInformationResponse> GetVehicleTableInformationAsync(GetVehicleTableInformationRequest request)
        {
            var totalOfVehicles = await vehicleRepository.GetTotalNumberOfVehiclesAsync();
            var vehiclesOnPage = await vehicleRepository.GetVehiclesOfThePageAsync(request.PageSize, request.PageNumber);

            return new GetVehicleTableInformationResponse(
                totalOfVehicles,
                vehiclesOnPage
            );
        }
    }
}