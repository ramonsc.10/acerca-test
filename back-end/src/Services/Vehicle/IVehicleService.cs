using System.Threading.Tasks;
using App.Models.Vehicles;

namespace App.Services.Vehicles
{
    public interface IVehicleService
    {
        Task<GetVehicleTableInformationResponse> GetVehicleTableInformationAsync(GetVehicleTableInformationRequest request);
        Task<CreateOrModifyVehicleResponse> CreateOrModifyVehicleAsync(CreateOrModifyVehicleRequest request);
        Task<GetVehicleInformationResponse> GetVehicleInformationAsync(GetVehicleInformationRequest request);
        Task DeleteVehicleAsync(DeleteVehicleRequest request);
    }
}