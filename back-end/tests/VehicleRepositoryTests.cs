using System;
using System.Collections.Generic;
using System.Linq;
using App.Context;
using App.Mappers.Vehicles;
using App.Models.Vehicles;
using App.Respositories.Vehicles;
using FluentAssertions;
using Xunit;

namespace App.Tests
{
    public class VehicleRepositoryTests
    {
        [Fact]
        public async void Given_NoPageSize_When_GetVehiclesOfThePage_Should_Return_AllRegisters()
        {
            var builderInMemory = new BuilderDbContextOptionsBuilder().AddConfigurationInMemory();
            var context = new ApplicationContext(builderInMemory.Options);
            var vehicleRepository = new VehicleRepository(context, new VehicleMapper());
            var vehicleGuid = Guid.NewGuid();
            var today = DateTime.UtcNow;
            var vehiclesToInsert = new List<VehicleEfModel>()
            {
                new VehicleEfModel(id: Guid.NewGuid(), orderNumber: 1, model: "model1", chassis: "chassis1",plate: "1234ABC", deliveryDate: today),
                new VehicleEfModel(id: Guid.NewGuid(), orderNumber: 2, model: "model2", chassis: "chassis2",plate: "1234ABC", deliveryDate: today),
                new VehicleEfModel(id: Guid.NewGuid(), orderNumber: 3, model: "model3", chassis: "chassis3",plate: "1234ABC", deliveryDate: today),
                new VehicleEfModel(id: Guid.NewGuid(), orderNumber: 4, model: "model4", chassis: "chassis4",plate: "1234ABC", deliveryDate: today),
                new VehicleEfModel(id: Guid.NewGuid(), orderNumber: 5, model: "model5", chassis: "chassis5",plate: "1234ABC", deliveryDate: today),
                new VehicleEfModel(id: Guid.NewGuid(), orderNumber: 6, model: "model6", chassis: "chassis6",plate: "1234ABC", deliveryDate: today),
            };
            context.AddRange(vehiclesToInsert);
            await context.SaveChangesAsync();

            var registers = await vehicleRepository.GetVehiclesOfThePageAsync(0, 0);

        }

        [Fact]
        public async void Given_PageSizeOf3_And_Page1_When_GetVehiclesOfThePage_Should_Return_First_3_Registers()
        {
            var builderInMemory = new BuilderDbContextOptionsBuilder().AddConfigurationInMemory();
            var context = new ApplicationContext(builderInMemory.Options);
            var vehicleRepository = new VehicleRepository(context, new VehicleMapper());
            var vehicleGuid = Guid.NewGuid();
            var today = DateTime.UtcNow;
            var vehiclesToInsert = new List<VehicleEfModel>()
            {
                new VehicleEfModel(id: Guid.NewGuid(), orderNumber: 1, model: "model1", chassis: "chassis1",plate: "1234ABC", deliveryDate: today),
                new VehicleEfModel(id: Guid.NewGuid(), orderNumber: 2, model: "model2", chassis: "chassis2",plate: "1234ABC", deliveryDate: today),
                new VehicleEfModel(id: Guid.NewGuid(), orderNumber: 3, model: "model3", chassis: "chassis3",plate: "1234ABC", deliveryDate: today),
                new VehicleEfModel(id: Guid.NewGuid(), orderNumber: 4, model: "model4", chassis: "chassis4",plate: "1234ABC", deliveryDate: today),
                new VehicleEfModel(id: Guid.NewGuid(), orderNumber: 5, model: "model5", chassis: "chassis5",plate: "1234ABC", deliveryDate: today),
                new VehicleEfModel(id: Guid.NewGuid(), orderNumber: 6, model: "model6", chassis: "chassis6",plate: "1234ABC", deliveryDate: today),
            };
            context.AddRange(vehiclesToInsert);
            await context.SaveChangesAsync();

            var registers = await vehicleRepository.GetVehiclesOfThePageAsync(3, 0);

            registers.Should().BeEquivalentTo(vehiclesToInsert.Take(3));
        }

        [Fact]
        public async void Given_PageSizeOf3_And_Page2_When_GetVehiclesOfThePage_Should_Return_Last_2_Registers()
        {
            var builderInMemory = new BuilderDbContextOptionsBuilder().AddConfigurationInMemory();
            var context = new ApplicationContext(builderInMemory.Options);
            var vehicleRepository = new VehicleRepository(context, new VehicleMapper());
            var vehicleGuid = Guid.NewGuid();
            var today = DateTime.UtcNow;
            var vehiclesToInsert = new List<VehicleEfModel>()
            {
                new VehicleEfModel(id: Guid.NewGuid(), orderNumber: 1, model: "model1", chassis: "chassis1",plate: "1234ABC", deliveryDate: today),
                new VehicleEfModel(id: Guid.NewGuid(), orderNumber: 2, model: "model2", chassis: "chassis2",plate: "1234ABC", deliveryDate: today),
                new VehicleEfModel(id: Guid.NewGuid(), orderNumber: 3, model: "model3", chassis: "chassis3",plate: "1234ABC", deliveryDate: today),
                new VehicleEfModel(id: Guid.NewGuid(), orderNumber: 4, model: "model4", chassis: "chassis4",plate: "1234ABC", deliveryDate: today),
                new VehicleEfModel(id: Guid.NewGuid(), orderNumber: 5, model: "model5", chassis: "chassis5",plate: "1234ABC", deliveryDate: today),
            };
            context.AddRange(vehiclesToInsert);
            await context.SaveChangesAsync();

            var registers = await vehicleRepository.GetVehiclesOfThePageAsync(3, 1);

            registers.Should().BeEquivalentTo(vehiclesToInsert.Skip(3).Take(3));
        }
    }
}