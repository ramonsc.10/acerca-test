using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.Models.Vehicles;
using App.Respositories.Vehicles;
using App.Services.Vehicles;
using FluentAssertions;
using Moq;
using Xunit;

namespace App.Tests
{
    public class VehicleServiceTests
    {
        [Fact]
        public async void Given_NoPageSize_When_GetVehicleTableInformationAsync_Should_Return_TotalPagesNull_And_AllRegisters()
        {
            var today = DateTime.UtcNow;
            var vehicles = new List<Vehicle>()
            {
                new Vehicle(id: Guid.NewGuid(), orderNumber: 1, model: "model1", chassis: "chassis1",plate: "1234ABC", deliveryDate: today),
                new Vehicle(id: Guid.NewGuid(), orderNumber: 2, model: "model2", chassis: "chassis2",plate: "1234ABC", deliveryDate: today),
                new Vehicle(id: Guid.NewGuid(), orderNumber: 3, model: "model3", chassis: "chassis3",plate: "1234ABC", deliveryDate: today),
                new Vehicle(id: Guid.NewGuid(), orderNumber: 4, model: "model4", chassis: "chassis4",plate: "1234ABC", deliveryDate: today),
                new Vehicle(id: Guid.NewGuid(), orderNumber: 5, model: "model5", chassis: "chassis5",plate: "1234ABC", deliveryDate: today),
                new Vehicle(id: Guid.NewGuid(), orderNumber: 6, model: "model6", chassis: "chassis6",plate: "1234ABC", deliveryDate: today),
            };
            var vehicleRepository = new Mock<IVehicleRepository>();
            vehicleRepository
                .Setup(repo => repo.GetTotalNumberOfVehiclesAsync())
                .ReturnsAsync(
                    vehicles.Count()
                );
            vehicleRepository
                .Setup(repo => repo.GetVehiclesOfThePageAsync(It.IsAny<int>(), It.IsAny<int>()))
                .Returns<int, int>((pageSize, pageNumber) =>
                {
                    return Task.Run(() =>
                    {
                        if (pageSize > 0)
                        {
                            return vehicles.Skip(pageSize * pageNumber).Take(pageSize);
                        }
                        return vehicles;
                    });
                });
            var vehicleService = new VehicleService(vehicleRepository.Object);

            var response = await vehicleService.GetVehicleTableInformationAsync(new GetVehicleTableInformationRequest());

            response.Should().BeEquivalentTo(
                new GetVehicleTableInformationResponse(
                    6, vehicles
                )
            );
        }

        [Fact]
        public async void Given_PageSize3_And_PageNumber_2_When_GetVehicleTableInformationAsync_Should_Return_TotalPages2_And_Last2Registers()
        {
            var today = DateTime.UtcNow;
            var vehicles = new List<Vehicle>()
            {
                new Vehicle(id: Guid.NewGuid(), orderNumber: 1, model: "model1", chassis: "chassis1",plate: "1234ABC", deliveryDate: today),
                new Vehicle(id: Guid.NewGuid(), orderNumber: 2, model: "model2", chassis: "chassis2",plate: "1234ABC", deliveryDate: today),
                new Vehicle(id: Guid.NewGuid(), orderNumber: 3, model: "model3", chassis: "chassis3",plate: "1234ABC", deliveryDate: today),
                new Vehicle(id: Guid.NewGuid(), orderNumber: 4, model: "model4", chassis: "chassis4",plate: "1234ABC", deliveryDate: today),
                new Vehicle(id: Guid.NewGuid(), orderNumber: 5, model: "model5", chassis: "chassis5",plate: "1234ABC", deliveryDate: today),
            };
            var vehicleRepository = new Mock<IVehicleRepository>();
            vehicleRepository
                .Setup(repo => repo.GetTotalNumberOfVehiclesAsync())
                .ReturnsAsync(
                    vehicles.Count()
                );
            vehicleRepository
                .Setup(repo => repo.GetVehiclesOfThePageAsync(It.IsAny<int>(), It.IsAny<int>()))
                .Returns<int, int>((pageSize, pageNumber) =>
                {
                    return Task.Run(() =>
                    {
                        if (pageSize > 0)
                        {
                            return vehicles.Skip(pageSize * pageNumber).Take(pageSize);
                        }
                        return vehicles;
                    });
                });
            var vehicleService = new VehicleService(vehicleRepository.Object);

            var response = await vehicleService.GetVehicleTableInformationAsync(
                new GetVehicleTableInformationRequest() { PageSize = 3, PageNumber = 1 }
            );

            response.Should().BeEquivalentTo(
                new GetVehicleTableInformationResponse(
                    5, vehicles.Skip(3).Take(3)
                )
            );
        }

    }
}