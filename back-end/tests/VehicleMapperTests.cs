using System;
using App.Mappers.Vehicles;
using App.Models.Vehicles;
using FluentAssertions;
using Xunit;

namespace App.Tests
{
    public class VehicleMapperTests
    {
        [Fact]
        public void Given_VehicleEfModel_When_From_Should_Return_Vehicle()
        {
            var mapper = new VehicleMapper();
            var vehicleGuid = Guid.NewGuid();
            var today = DateTime.UtcNow;
            var dbRegister = new VehicleEfModel(
                id: vehicleGuid,
                orderNumber: 1,
                model: "model123",
                chassis: "chassis123",
                plate: "1234ABC",
                deliveryDate: today
            );

            var vehicle = mapper.From(dbRegister);

            vehicle.Should().BeEquivalentTo(
                new Vehicle(
                    id: vehicleGuid,
                    orderNumber: 1,
                    model: "model123",
                    chassis: "chassis123",
                    plate: "1234ABC",
                    deliveryDate: today
                )
            );
        }
        
        [Fact]
        public void Given_UpsertVehicle_When_From_Should_Return_VehicleEfModel()
        {
            var mapper = new VehicleMapper();
            var vehicleGuid = Guid.NewGuid();
            var today = DateTime.UtcNow;
            var upsertVehicle = new UpsertVehicle(
                id: vehicleGuid,
                orderNumber: 1,
                model: "model123",
                chassis: "chassis123",
                plate: "1234ABC",
                deliveryDate: today
            );

            var dbRegister = mapper.From(upsertVehicle);

            dbRegister.Should().BeEquivalentTo(
                new VehicleEfModel(
                    id: vehicleGuid,
                    orderNumber: 1,
                    model: "model123",
                    chassis: "chassis123",
                    plate: "1234ABC",
                    deliveryDate: today
                )
            );
        }

    }
}