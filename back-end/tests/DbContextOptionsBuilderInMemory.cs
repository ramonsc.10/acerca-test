using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;

namespace App.Tests
{
    public class BuilderDbContextOptionsBuilder
    {
        private DbContextOptionsBuilder builder;
        
        public BuilderDbContextOptionsBuilder()
        {
            builder = new DbContextOptionsBuilder();

        }

        public Microsoft.EntityFrameworkCore.DbContextOptionsBuilder AddConfigurationInMemory()
        {
            builder.UseInMemoryDatabase(Guid.NewGuid().ToString()).ConfigureWarnings(w =>
            {
                w.Ignore(InMemoryEventId.TransactionIgnoredWarning);
            });
            return builder;
        }
    }
}