#!/bin/sh

envsubst '$PORT' < /nginx-container-data/nginx.template.conf > /etc/nginx/conf.d/default.conf
nginx -g "daemon off;"
