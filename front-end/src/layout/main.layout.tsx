import React, { FC } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { makeStyles } from '@material-ui/core';
import { VehiclePage } from '../pages/vehicle/vehicle.page';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex'
    }
}));

const MainLayout: FC = () => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            {
                < BrowserRouter basename={process.env.REACT_APP_BASENAME}>
                    <Switch>
                        <Route path="/" component={VehiclePage} />
                    </Switch>
                </BrowserRouter>
            }
        </div >
    );
}

export default MainLayout;
