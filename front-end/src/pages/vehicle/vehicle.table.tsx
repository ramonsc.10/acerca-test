import React, { FC, useState } from 'react';
import { Table, TableHead, TableRow, TableCell, TableBody, TableContainer, TablePagination, Menu, MenuItem } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { Vehicle } from '../../models/vehicle';
import { VehicleRow } from './vehicle.row';

interface VehicleTableProps {
    items: Vehicle[];
    totalItems: number;
    page: number;
    rowsPerPage: number;
    onChangePage: (page: number) => void;
    onChangeRowsPerPage: (rowsPerPage: number) => void;
    onVehicleToEdit: (id: string) => void;
    onVehicleToRemove: (id: string) => void;
}

export const VehicleTable: FC<VehicleTableProps> = ({ items, page, onChangePage, rowsPerPage, onChangeRowsPerPage, totalItems, onVehicleToEdit, onVehicleToRemove }) => {
    const { t } = useTranslation();
    const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
    const [vehicleId, setVehicleId] = useState<string>('');

    const handleChangePage = (event: React.MouseEvent<HTMLButtonElement> | null, page: number) => {
        onChangePage(page);
    };
    const handleChangeRowsPerPage = (
        event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    ) => {
        onChangeRowsPerPage(parseInt(event.target.value, 10));
        onChangePage(0);
    };

    const handleClickItemMenu = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>, id: string) => {
        setAnchorEl(event.currentTarget);
        setVehicleId(id);
    };

    const handleClickEdit = () => {
        onVehicleToEdit(vehicleId);
        setVehicleId('');
        handleCloseMenu();
    };

    const handleClickRemove = () => {
        onVehicleToRemove(vehicleId);
        setVehicleId('');
        handleCloseMenu();
    };

    const handleCloseMenu = () => {
        setAnchorEl(null);
    };

    return (
        <>
            <TableContainer >
                <Table size='small' aria-label="simple table" stickyHeader>
                    <TableHead>
                        <TableRow>
                            <TableCell variant='head' ><b>{t('vehicle.table.header.orderNumber')}</b></TableCell>
                            <TableCell variant='head' ><b>{t('vehicle.table.header.chassis')}</b></TableCell>
                            <TableCell variant='head' ><b>{t('vehicle.table.header.model')}</b></TableCell>
                            <TableCell variant='head' ><b>{t('vehicle.table.header.plate')}</b></TableCell>
                            <TableCell variant='head' ><b>{t('vehicle.table.header.deliveryDate')}</b></TableCell>
                            <TableCell variant='head' ></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {items
                            .map(item => (
                                <VehicleRow item={item} key={item.id} onClickItemMenu={handleClickItemMenu} />
                            ))
                        }
                    </TableBody >
                </Table >
            </TableContainer>
            <TablePagination
                rowsPerPageOptions={[10, 50, { value: totalItems, label: 'Todos' }]}
                component={"div"}
                count={totalItems}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
            />
            <Menu
                id="long-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleCloseMenu}
                transformOrigin={{ vertical: 'top', horizontal: 'left' }}
                PaperProps={{
                    style: {
                        width: '20ch',
                    },
                }}
            >
                <MenuItem onClick={handleClickEdit}>
                    {t('vehicle.table.menu.edit')}
                </MenuItem>
                <MenuItem onClick={handleClickRemove}>
                    {t('vehicle.table.menu.remove')}
                </MenuItem>
            </Menu>
        </>);
}