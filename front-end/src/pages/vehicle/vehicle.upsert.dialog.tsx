import { Button, Dialog, DialogActions, DialogContent, DialogTitle, makeStyles, TextField } from '@material-ui/core';
import { DatePicker } from '@material-ui/pickers';
import React, { FC, useState } from 'react';
import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import useIoC from '../../contexts/ioc.context';
import { VehicleGateway } from '../../gateways/vehicle.gateway';
import { initUpsertVehicle, UpsertVehicle } from '../../models/vehicle';

const useStyles = makeStyles((theme) => ({
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
}));

interface VehicleUpsertDialogProps {
    open: boolean;
    id?: string;
    onUpsert: (item: UpsertVehicle) => void
    onClose: () => void;
}

export const VehicleUpsertDialog: FC<VehicleUpsertDialogProps> = ({ open, id, onUpsert, onClose }) => {
    const classes = useStyles();
    const { t } = useTranslation();
    const [upsertVehicle, setUpsertVehicle] = useState<UpsertVehicle>(initUpsertVehicle());
    const [errorText, setErrorText] = useState<string>();
    const vehicleGateway = useIoC<VehicleGateway>(VehicleGateway);

    useEffect(() => {
        if (!open) return;
        if (id) {
            (async () => {
                const response = await vehicleGateway.GetVehicleInformationAsync({
                    id: id
                });
                setUpsertVehicle(response.vehicle);
            })();
        } else {
            setUpsertVehicle(initUpsertVehicle());
        }
    }, [open])

    const handleClickSave = () => {
        if (!upsertVehicle.chassis && !upsertVehicle.model && !upsertVehicle.plate) {
            setErrorText(t('vehicle.dialog.form.error'));
            return;
        }
        onUpsert(upsertVehicle);
        onClose();
    }

    return (
        <Dialog open={open} onClose={onClose} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">{t('vehicle.dialog.title')}</DialogTitle>
            <DialogContent>
                <TextField
                    required
                    name={'orderNumber'}
                    type={'number'}
                    label={t('vehicle.dialog.form.orderNumber')}
                    variant='outlined'
                    margin='dense'
                    className={classes.textField}
                    style={{ width: 250 }}
                    onChange={(event) => { upsertVehicle.orderNumber = Number(event.target.value); setUpsertVehicle({ ...upsertVehicle }); }}
                    value={upsertVehicle.orderNumber || 0}
                />
                <TextField
                    error={Boolean(errorText) && !Boolean(upsertVehicle.chassis)}
                    required
                    type={'text'}
                    label={t('vehicle.dialog.form.chassis')}
                    variant='outlined'
                    margin='dense'
                    className={classes.textField}
                    style={{ width: 250 }}
                    onChange={(event) => { upsertVehicle.chassis = event.target.value; setUpsertVehicle({ ...upsertVehicle }); }}
                    value={upsertVehicle.chassis || ''}
                    helperText={errorText}
                />
                <TextField
                    error={Boolean(errorText) && !Boolean(upsertVehicle.model)}
                    required
                    type={'text'}
                    label={t('vehicle.dialog.form.model')}
                    variant='outlined'
                    margin='dense'
                    className={classes.textField}
                    style={{ width: 250 }}
                    onChange={(event) => { upsertVehicle.model = event.target.value; setUpsertVehicle({ ...upsertVehicle }); }}
                    value={upsertVehicle.model || ''}
                    helperText={errorText}
                />
                <TextField
                    error={Boolean(errorText) && !Boolean(upsertVehicle.plate)}
                    required
                    type={'text'}
                    label={t('vehicle.dialog.form.plate')}
                    variant='outlined'
                    margin='dense'
                    className={classes.textField}
                    style={{ width: 250 }}
                    onChange={(event) => { upsertVehicle.plate = event.target.value; setUpsertVehicle({ ...upsertVehicle }); }}
                    value={upsertVehicle.plate || ''}
                    helperText={errorText}
                />

                <DatePicker
                    label={t('vehicle.dialog.form.deliveryDate')}
                    format='dd/MM/yyyy'
                    inputVariant='outlined'
                    margin='dense'
                    onChange={(newDate) => { upsertVehicle.deliveryDate = newDate as Date; setUpsertVehicle({ ...upsertVehicle }); }}
                    value={upsertVehicle.deliveryDate || new Date()}
                    className={classes.textField || ''}
                />

            </DialogContent>
            <DialogActions>
                <Button onClick={onClose} color="primary">
                    {t('vehicle.dialog.buttons.cancel')}
                </Button>
                <Button onClick={handleClickSave} color="primary">
                    {t('vehicle.dialog.buttons.save')}
                </Button>
            </DialogActions>
        </Dialog>
    );
}