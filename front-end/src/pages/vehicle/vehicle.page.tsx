import { Fab, Grid, makeStyles } from "@material-ui/core";
import React, { FC, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import useIoC from "../../contexts/ioc.context";
import { VehicleGateway } from "../../gateways/vehicle.gateway";
import { UpsertVehicle, Vehicle } from "../../models/vehicle";
import { VehicleTable } from "./vehicle.table";
import { VehicleUpsertDialog } from "./vehicle.upsert.dialog";
import AddIcon from '@material-ui/icons/Add';

const useStyles = makeStyles((theme) => ({
    fab: {
        position: 'fixed',
        bottom: theme.spacing(2),
        right: theme.spacing(2),
    },
}));

export const VehiclePage: FC = () => {
    const { t } = useTranslation();
    const classes = useStyles();
    const [vehicles, setVehicles] = useState<Vehicle[]>([]);
    const [totalItems, setTotalItems] = useState<number>(0);
    const [page, setPage] = useState<number>(0);
    const [rowsPerPage, setRowsPerPage] = useState<number>(10);
    const [showDialog, setShowDialog] = useState<boolean>(false);
    const [vehicleId, setVehicleId] = useState<string>();

    const vehicleGateway = useIoC<VehicleGateway>(VehicleGateway);

    useEffect(() => {
        updateTable();
    }, [page, rowsPerPage]);

    const updateTable = () => {
        (async () => {
            const response = await vehicleGateway.GetVehicleTableInformationAsync({ pageNumber: page, pageSize: rowsPerPage });
            setTotalItems(response.totalItems);
            setVehicles(response.vehicles);
        })()
    };

    const handleUpsert = (item: UpsertVehicle) => {
        (async () => {
            await vehicleGateway.CreateOrModifyVehicleAsync({
                upsertVehicle: item
            });
            updateTable();
        })();
    }

    const handleEditVehicle = (id: string) => {
        setVehicleId(id);
        setShowDialog(true);
    }

    const handleRemoveVehicle = (id: string) => {
        (async () => {
            await vehicleGateway.RemoveVehicleAsync({
                id: id
            });
            updateTable();
        })();
    }

    return (
        <Grid container>
            <VehicleTable
                items={vehicles}
                totalItems={totalItems}
                page={page}
                rowsPerPage={rowsPerPage}
                onChangePage={(newPage: number) => { setPage(newPage) }}
                onChangeRowsPerPage={(newRowsPerPage: number) => { setRowsPerPage(newRowsPerPage) }}
                onVehicleToEdit={handleEditVehicle}
                onVehicleToRemove={handleRemoveVehicle}
            />
            <VehicleUpsertDialog
                open={showDialog}
                id={vehicleId}
                onUpsert={handleUpsert}
                onClose={() => { setShowDialog(false); setVehicleId(undefined); }}
            />
            <Fab size={'large'} aria-label="add" className={classes.fab} onClick={() => { setShowDialog(true); }}>
                <AddIcon />
            </Fab>
        </Grid>
    );
}