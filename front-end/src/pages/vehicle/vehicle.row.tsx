import { IconButton, TableCell, TableRow } from "@material-ui/core";
import moment from "moment";
import React, { FC } from 'react';
import { Vehicle } from "../../models/vehicle";
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';

interface VehicleRowProps {
    item: Vehicle;
    onClickItemMenu: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>, id: string) => void
}
export const VehicleRow: FC<VehicleRowProps> = ({ item, onClickItemMenu }) => {

    return (
        <TableRow key={item.id}>
            <TableCell variant='body'> {item.orderNumber} </TableCell>
            <TableCell variant='body'> {item.chassis}</TableCell>
            <TableCell variant='body'> {item.model}</TableCell>
            <TableCell variant='body'> {item.plate}</TableCell>
            <TableCell variant='body'> {moment(item.deliveryDate).format('DD/MM/YYYY')}</TableCell>
            <TableCell variant='body'>
                <IconButton onClick={(event) => { onClickItemMenu(event, item.id); }}>
                    <MoreHorizIcon />
                </IconButton>
            </TableCell>
        </TableRow>
    )
}