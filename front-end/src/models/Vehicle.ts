export interface Vehicle {
    id: string;
    orderNumber: number;
    model: string;
    chassis: string;
    plate: string;
    deliveryDate: Date;
}

export interface UpsertVehicle {
    id?: string;
    orderNumber: number;
    model: string;
    chassis: string;
    plate: string;
    deliveryDate: Date;
}

export function initUpsertVehicle(): UpsertVehicle {

    return {
        orderNumber: 0,
        model: '',
        chassis: '',
        plate: '',
        deliveryDate: new Date()
    }
}