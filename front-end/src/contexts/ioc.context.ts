import { Container } from "aurelia-dependency-injection";
import buildAxiosInstance from "../gateways/axios.builder";
import { createContext, useContext } from "react";

const ioc = new Container()
    .makeGlobal();

ioc.registerInstance('axios', buildAxiosInstance());

const IoCContext = createContext(ioc);

const useContextIoC = () => useContext(IoCContext);

function useIoC<TType>(key: any) {
    return useContextIoC().get(key) as TType;
}

export default useIoC;