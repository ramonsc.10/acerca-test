import React, { Suspense } from 'react';
import './App.css';
import { CssBaseline } from '@material-ui/core';
import { Main } from './layout/main';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import esLocale from 'date-fns/locale/es';

const App: React.FC = () => {
	return (
		<Suspense fallback=''>
			<MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
				<CssBaseline />
				<Main />
			</MuiPickersUtilsProvider>

		</Suspense>
	);
};

export default App;
