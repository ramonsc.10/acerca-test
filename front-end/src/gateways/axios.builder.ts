import Axios from "axios"

const buildAxiosInstance = () => {
    const instance = Axios.create({
        baseURL: process.env.REACT_APP_API_URL
    });

    return instance;
}

export default buildAxiosInstance;
