import { inject } from 'aurelia-dependency-injection';
import { AxiosInstance } from 'axios';
import querystring from 'query-string'
import { CreateOrModifyVehicleRequest, CreateOrModifyVehicleResponse, GetVehicleInformationRequest, GetVehicleInformationResponse, GetVehicleTableInformationRequest, GetVehicleTableInformationResponse } from './vehicle.gateway.interface';

@inject('axios')
export class VehicleGateway {
    private readonly uri = 'vehicle';

    constructor(
        private axios: AxiosInstance
    ) { }


    async GetVehicleTableInformationAsync(request: GetVehicleTableInformationRequest): Promise<GetVehicleTableInformationResponse> {
        let url = this.uri + '/Table';

        var query = querystring.stringify(request);
        url += `?${query}`;

        const response = await this.axios.get(url);

        return response.data;
    }

    async GetVehicleInformationAsync(request: GetVehicleInformationRequest): Promise<GetVehicleInformationResponse> {
        let url = this.uri

        var query = querystring.stringify(request);
        url += `?${query}`;

        const response = await this.axios.get(url);

        return response.data;
    }

    async RemoveVehicleAsync(request: GetVehicleInformationRequest): Promise<GetVehicleInformationResponse> {
        let url = this.uri

        var query = querystring.stringify(request);
        url += `?${query}`;

        const response = await this.axios.delete(url);

        return response.data;
    }


    async CreateOrModifyVehicleAsync(request: CreateOrModifyVehicleRequest): Promise<CreateOrModifyVehicleResponse> {
        const response = await this.axios.post(this.uri, request);

        return response.data;
    }
}