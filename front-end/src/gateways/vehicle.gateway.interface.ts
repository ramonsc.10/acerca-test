import { UpsertVehicle, Vehicle } from "../models/vehicle";

export interface GetVehicleTableInformationRequest {
    pageSize?: number;
    pageNumber: number;
}

export interface GetVehicleTableInformationResponse {
    totalItems: number;
    vehicles: Vehicle[];
}

export interface CreateOrModifyVehicleRequest {
    upsertVehicle: UpsertVehicle;
}

export interface CreateOrModifyVehicleResponse {
    vehicle: Vehicle;
}

export interface GetVehicleInformationRequest {
    id: string;
}

export interface GetVehicleInformationResponse {
    vehicle: Vehicle;
}