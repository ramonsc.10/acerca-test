# app
## Requirements

dotnet-sdk-3.1.300 x64 | x86<br />
Docker Desktop

## Build 

docker-compose up --build

## Run
Application back-end will execute on http://localhost:3450/
Application front-end will execute on http://localhost:3000/

# Docker Support
All apps have a Dockerfile that you can build a execute independently or you can use the docker-compose in order to run any of the services you want. The services available are:
1. A postgresql database named `database`
1. A service to execute the migrations in the `database` service named `db_migrations`
1. The `back_end` application
1. The `front-end` application

## Database Setup
**NOTE:** There aren't configured any volumes for the services so anytime you shut down the services, all the data is deleted. If it's necessary, request the team or implement the volume feature

In order to run the database you have to:
1. Execute the command `docker-compose up database [any other service you want]`