FROM mcr.microsoft.com/dotnet/core/sdk:3.1.301-alpine3.12
WORKDIR /src

ENV PATH "$PATH:/root/.dotnet/tools"

RUN dotnet tool install --global dotnet-ef --version 3.1.5

COPY ./back-end/src/app.csproj .
RUN dotnet restore 

COPY ./back-end/src .

RUN dotnet build --configuration Release

ENTRYPOINT [ "dotnet", "ef","database", "update", "--configuration", "Release", "-v" ]